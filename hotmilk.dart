import 'hotdrink.dart';

class hotmilk extends hotdrink {
  hotmilk(super.Menuname, super.price, super.sweetlevel);

  @override
  void showmenu() {
    print("hotdrink : hot_milk");
  }

  @override
  void addhotwater() {
    print("milk add hot water");
  }
}
