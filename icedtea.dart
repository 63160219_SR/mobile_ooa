import 'colddrink.dart';

class icedtea extends colddrink {
  icedtea(super.Menuname, super.price, super.sweetlevel);

  @override
  void showmenu() {
    print("colddrink : iced_tea");
  }

  @override
  void addice() {
    print("tea add ice");
  }
}