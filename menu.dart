import 'dart:io';

abstract class menu {
  String? menuname;
  int price = 0;
  int sweetlevel = 0;
  int smoothie = 0;

  menu(String menuname, int price, int sweetlevel) {
    this.menuname = menuname;
    this.price = price;
    this.sweetlevel = sweetlevel;
  }

  void readorder() {
    print("menuname: " +
        this.menuname.toString() +
        "\n" +
        "price: " +
        this.price.toString() +
        " ฿" +
        "\n" +
        "sweetlevel: " +
        this.sweetlevel.toString() +
        " %");
  }

  void showmenu() {}
  void addhotwater() {}
  void addice() {}

  int smoothieprice() {
    price = price + 10;
    return price;
  }
}
