import 'dart:io';

import 'Protein.dart';
import 'coldmilk.dart';
import 'hotTea.dart';
import 'hotcoffee.dart';
import 'hotmilk.dart';
import 'icedcoffee.dart';
import 'icedtea.dart';
import 'soda.dart';

void main(List<String> args) {
  print("TAOBIN");
  print("input 1 = hotdrink");
  print("input 2 = colddrink");
  print("input 3 = other");
  print(" Enter your Number ");
  int? menu = int.parse(stdin.readLineSync()!);
    if (menu == 1) {
      print("HOT_DRINK");
      print("1. HOT_COFFEE");
      print("2. HOT_TEA");
      print("3. HOT_MILK,COCOA,CARAMEL" + "\n");
      print("input number : ");
      int HDnum = int.parse(stdin.readLineSync()!);
      if (HDnum == 1) {
        print("1. Espresso    45 ฿"
            "\n"
            "2. Hot Cafe'Latte    45 ฿"
            "\n"
            "3. Hot Americano   45 ฿"
            "\n");
        print("input number menu :");
        int HCnum = int.parse(stdin.readLineSync()!);
        print("input sweetLevel : 100 , 70 , 50 , 20 , 0");
        int? sw_level = int.parse(stdin.readLineSync()!);
        switch (HCnum) {
          case 1:
            hotcoffee h1 = hotcoffee("Espresso", 45, sw_level);
            h1.showmenu();
            h1.addhotwater();
            h1.readorder();
            showconfirn();
            break;
          case 2:
            hotcoffee h1 = hotcoffee("Hot Cafe'Latte", 45, sw_level);
            h1.showmenu();
            h1.addhotwater();
            h1.readorder();
            showconfirn();
            break;
          case 3:
            hotcoffee h1 = hotcoffee("Hot Americano", 45, sw_level);
            h1.showmenu();
            h1.addhotwater();
            h1.readorder();
            showconfirn();
            break;
        }
      } else if (HDnum == 2) {
        print("1. Hot Thai Milk Tea   40 ฿"
            "\n"
            "2. Hot Taiwanese Tea   40 ฿"
            "\n"
            "3. Hot Matcha Latte    40 ฿"
            "\n");
        print("input number menu :");
        int HCnum = int.parse(stdin.readLineSync()!);
        print("input sweetLevel : 100 , 70 , 50 , 20 , 0");
        int? sw_level = int.parse(stdin.readLineSync()!);
        switch (HCnum) {
          case 1:
            hottea h1 = hottea("Hot Thai Milk Tea", 40, sw_level);
            h1.showmenu();
            h1.addhotwater();
            h1.readorder();
            showconfirn();
            break;
          case 2:
            hottea h1 = hottea("Hot Taiwanese Tea", 40, sw_level);
            h1.showmenu();
            h1.addhotwater();
            h1.readorder();
            showconfirn();
            break;
          case 3:
            hottea h1 = hottea("Hot Matcha Latte", 40, sw_level);
            h1.showmenu();
            h1.addhotwater();
            h1.readorder();
            showconfirn();
            break;
        }
      } else {
        print("1. Hot Caramel Milk    40 ฿"
            "\n"
            "2. Hot Caramel Cocoa    40 ฿"
            "\n"
            "3. Hot Cocoa   40 ฿"
            "\n");
        print("input number menu :");
        int HCnum = int.parse(stdin.readLineSync()!);
        print("input sweetLevel : 100 , 70 , 50 , 20 , 0");
        int? sw_level = int.parse(stdin.readLineSync()!);
        switch (HCnum) {
          case 1:
            hotmilk h1 = hotmilk("Hot Caramel Milk", 40, sw_level);
            h1.showmenu();
            h1.addhotwater();
            h1.readorder();
            showconfirn();
            break;
          case 2:
            hotmilk h1 = hotmilk("Hot Caramel Cocoa", 40, sw_level);
            h1.showmenu();
            h1.addhotwater();
            h1.readorder();
            showconfirn();
            break;
          case 3:
            hotmilk h1 = hotmilk("Hot Cocoa", 40, sw_level);
            h1.showmenu();
            h1.addhotwater();
            h1.readorder();
            showconfirn();
            break;
        }
      }
    }
    if (menu == 2) {
      print("COLD_DRINK");
      print("1. ICED_COFFEE");
      print("2. ICED_TEA");
      print("3. COLD_MILK,COCOA,CARAMEL" + "\n");
      print("input number : ");
      int HDnum = int.parse(stdin.readLineSync()!);
      if (HDnum == 1) {
        print("1. Iced Americano    50 ฿"
            "\n"
            "2. Iced Mocha    50 ฿"
            "\n"
            "3. Iced Espresso   50 ฿"
            "\n");
        print("input number menu :");
        int HCnum = int.parse(stdin.readLineSync()!);
        print("input sweetLevel : 100 , 70 , 50 , 20 , 0");
        int? sw_level = int.parse(stdin.readLineSync()!);
        switch (HCnum) {
          case 1:
            icedcoffee h1 = icedcoffee("Iced Americano", 50, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
          case 2:
            icedcoffee h1 = icedcoffee("Iced Mocha", 50, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
          case 3:
            icedcoffee h1 = icedcoffee("Iced Espresso", 50, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
        }
      } else if (HDnum == 2) {
        print("1. Iced Thai Milk Tea    45 ฿"
            "\n"
            "2. Iced Taiwanese Milk Tea   45 ฿"
            "\n"
            "3. Iced Matcha Latte   45 ฿"
            "\n");
        print("input number menu :");
        int HCnum = int.parse(stdin.readLineSync()!);
        print("input sweetLevel : 100 , 70 , 50 , 20 , 0");
        int? sw_level = int.parse(stdin.readLineSync()!);
        switch (HCnum) {
          case 1:
            icedtea h1 = icedtea("Iced Thai Milk Tea", 45, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
          case 2:
            icedtea h1 = icedtea("Iced Taiwanese Milk Tea", 45, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
          case 3:
            icedtea h1 = icedtea("Iced Matcha Latte", 45, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
        }
      } else {
        print("1. Iced Pink Milk    45 ฿"
            "\n"
            "2. Iced Caramel Milk   45 ฿"
            "\n"
            "3. Iced Matcha Latte   45 ฿"
            "\n");
        print("input number menu :");
        int HCnum = int.parse(stdin.readLineSync()!);
        print("input sweetLevel : 100 , 70 , 50 , 20 , 0");
        int? sw_level = int.parse(stdin.readLineSync()!);
        switch (HCnum) {
          case 1:
            coldmilk h1 = coldmilk("Iced Pink Milk", 45, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
          case 2:
            coldmilk h1 = coldmilk("Iced Caramel Milk", 45, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
          case 3:
            coldmilk h1 = coldmilk("Iced Matcha Latte", 45, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
        }
      }
    }
    if (menu == 3) {
      print("OTHER");
      print("1. PROTEIN");
      print("2. SODA"+"\n");
      print("input number : ");
      int HDnum = int.parse(stdin.readLineSync()!);
      if (HDnum == 1) {
        print("1. Chocolate Protein Shakes    70 ฿"
            "\n"
            "2. Stawberry Protein Shakes    70 ฿"
            "\n"
            "3. Caramel Protein Shakes    70 ฿"
            "\n");
        print("input number menu :");
        int HCnum = int.parse(stdin.readLineSync()!);
        print("input sweetLevel : 30 , 20 , 0");
        int? sw_level = int.parse(stdin.readLineSync()!);
        switch (HCnum) {
          case 1:
            protein h1 = protein("Chocolate Protein Shakes", 70, sw_level);
            h1.showmenu();
            h1.readorder();
            showconfirn();
            break;
          case 2:
            protein h1 = protein("Stawberry Protein Shakes", 70, sw_level);
            h1.showmenu();
            h1.readorder();
            showconfirn();
            break;
          case 3:
            protein h1 = protein("Caramel Protein Shakes", 70, sw_level);
            h1.showmenu();
            h1.readorder();
            showconfirn();
            break;
        }
      } else{
        print("1. Pepsi   15 ฿"
            "\n"
            "2. Iced Strawberry Soda    35 ฿"
            "\n"
            "3. Iced Blueberry Soda   35 ฿"
            "\n");
        print("input number menu :");
        int HCnum = int.parse(stdin.readLineSync()!);
        print("input sweetLevel : 100 , 50 , 20");
        int? sw_level = int.parse(stdin.readLineSync()!);
        switch (HCnum) {
          case 1:
            soda h1 = soda("Pepsi", 15, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.addice();
            h1.readorder();
            showconfirn();
            break;
          case 2:
            soda h1 = soda("Iced Strawberry Soda", 35, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.readorder();
            showconfirn();
            break;
          case 3:
            soda h1 = soda("Iced Blueberry Soda", 35, sw_level);
            reqsmoothie();
            String SM = (stdin.readLineSync()!);
            if (SM == "y") {
              h1.smoothieprice();
            }
            h1.showmenu();
            h1.readorder();
            showconfirn();
            break;
        }
      }
    }
  }

void reqsmoothie() {
  print("Smoothie? +10");
  print("input y if you want a smoothieใ" +
      "\n" +
      "input n if you don't want it.");
}


void showconfirn() {
  print("---------------------------------");
  print("confirm order and pay");
  print("Thank you for using the service");
  print("---------------------------------");
}
