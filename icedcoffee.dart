import 'colddrink.dart';

class icedcoffee extends colddrink {
  icedcoffee(super.Menuname, super.price, super.sweetlevel);

  @override
  void showmenu() {
    print("colddrink : iced_coffee");
  }

  @override
  void addice() {
    print("coffee add ice");
  }
}